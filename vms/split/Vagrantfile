# -*- mode: ruby -*-
# vi: set ft=ruby :

VAGRANTFILE_API_VERSION = "2"

Vagrant.configure(VAGRANTFILE_API_VERSION) do |config|


  if Vagrant.has_plugin?("vagrant-hosts")
    config.vm.provision :hosts do |provisioner|
      provisioner.add_localhost_hostnames = false
      provisioner.autoconfigure = true
      provisioner.sync_hosts = true
      provisioner.add_host '192.168.250.20', ['puppet']
      provisioner.add_host '192.168.250.25', ['puppetdb']
    end
  end

  config.vm.box = "gutocarvalho/centos7x64"

  # puppet server + puppet agent
  config.vm.define "puppetserver" do |puppetserver|
    puppetserver.vm.hostname = "puppetserver.hacklab"
    puppetserver.vm.network :private_network, ip: "192.168.250.20"
    puppetserver.vm.provision "shell", path: "installer.sh"
    puppetserver.vm.provider "virtualbox" do |v|
      v.customize [ "modifyvm", :id, "--cpus", "2" ]
      v.customize [ "modifyvm", :id, "--memory", "1024" ]
      v.customize [ "modifyvm", :id, "--name", "puppetserver.hacklab" ]
      v.customize [ "modifyvm", :id, "--groups", "/pcp" ]
    end
  end

  # puppet agent + puppetdb + puppet explorer
  config.vm.define "puppetdb" do |puppetdb|
    puppetdb.vm.hostname = "puppetdb.hacklab"
    puppetdb.vm.network :private_network, ip: "192.168.250.25"
    puppetdb.hostsupdater.aliases = ["puppetboard.hacklab", "puppetexplorer.hacklab"]
    puppetdb.vm.provider "virtualbox" do |v|
      v.customize [ "modifyvm", :id, "--cpus", "2" ]
      v.customize [ "modifyvm", :id, "--memory", "1024" ]
      v.customize [ "modifyvm", :id, "--name", "puppetdb.hacklab" ]
      v.customize [ "modifyvm", :id, "--groups", "/pcp" ]
    end
    puppetdb.vm.provision "puppet_server" do |puppet|
      puppet.puppet_server = "puppetserver.hacklab"
      puppet.puppet_node = "puppetdb.hacklab"
      puppet.options = "--verbose"
    end
  end

  #  puppet agent + mcollective client + activemq
  config.vm.define "puppetmq" do |puppetmq|
    puppetmq.vm.hostname = "puppetmq.hacklab"
    puppetmq.vm.network :private_network, ip: "192.168.250.30"
    puppetmq.vm.provider "virtualbox" do |v|
      v.customize [ "modifyvm", :id, "--cpus", "2" ]
      v.customize [ "modifyvm", :id, "--memory", "1024" ]
      v.customize [ "modifyvm", :id, "--name", "puppetmq.hacklab"]
      v.customize [ "modifyvm", :id, "--groups", "/pcp" ]
    end
    puppetmq.vm.provision "puppet_server" do |puppet|
      puppet.puppet_server = "puppetserver.hacklab"
      puppet.puppet_node = "puppetmq.hacklab"
      puppet.options = "--verbose"
    end
  end

end
